package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 申请权限的应用对象 open_app
 * 
 * @author ruoyi
 * @date 2023-02-26
 */
public class OpenApp extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 应用名称 */
    @Excel(name = "应用名称")
    private String appName;

    /** 应用描述 */
    @Excel(name = "应用描述")
    private String appDescription;

    /** 应用token */
    @Excel(name = "应用token")
    private String appToken;

    /** 添加时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "添加时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 添加人 */
    @Excel(name = "添加人")
    private String createdBy;

    /** 应用状态 */
    @Excel(name = "应用状态")
    private Long appStatus;

    /** 过期时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "过期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expireTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAppName(String appName) 
    {
        this.appName = appName;
    }

    public String getAppName() 
    {
        return appName;
    }
    public void setAppDescription(String appDescription) 
    {
        this.appDescription = appDescription;
    }

    public String getAppDescription() 
    {
        return appDescription;
    }
    public void setAppToken(String appToken) 
    {
        this.appToken = appToken;
    }

    public String getAppToken() 
    {
        return appToken;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setCreatedBy(String createdBy) 
    {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() 
    {
        return createdBy;
    }
    public void setAppStatus(Long appStatus) 
    {
        this.appStatus = appStatus;
    }

    public Long getAppStatus() 
    {
        return appStatus;
    }
    public void setExpireTime(Date expireTime) 
    {
        this.expireTime = expireTime;
    }

    public Date getExpireTime() 
    {
        return expireTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("appName", getAppName())
            .append("appDescription", getAppDescription())
            .append("appToken", getAppToken())
            .append("createdAt", getCreatedAt())
            .append("createdBy", getCreatedBy())
            .append("appStatus", getAppStatus())
            .append("expireTime", getExpireTime())
            .toString();
    }
}