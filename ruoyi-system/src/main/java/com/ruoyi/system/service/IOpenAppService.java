package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.OpenApp;

/**
 * 申请权限的应用Service接口
 * 
 * @author ruoyi
 * @date 2023-02-26
 */
public interface IOpenAppService 
{
    /**
     * 查询申请权限的应用
     * 
     * @param id 申请权限的应用主键
     * @return 申请权限的应用
     */
    public OpenApp selectOpenAppById(Long id);

    /**
     * 查询申请权限的应用列表
     * 
     * @param openApp 申请权限的应用
     * @return 申请权限的应用集合
     */
    public List<OpenApp> selectOpenAppList(OpenApp openApp);

    /**
     * 新增申请权限的应用
     * 
     * @param openApp 申请权限的应用
     * @return 结果
     */
    public int insertOpenApp(OpenApp openApp);

    /**
     * 修改申请权限的应用
     * 
     * @param openApp 申请权限的应用
     * @return 结果
     */
    public int updateOpenApp(OpenApp openApp);

    /**
     * 批量删除申请权限的应用
     * 
     * @param ids 需要删除的申请权限的应用主键集合
     * @return 结果
     */
    public int deleteOpenAppByIds(Long[] ids);

    /**
     * 删除申请权限的应用信息
     * 
     * @param id 申请权限的应用主键
     * @return 结果
     */
    public int deleteOpenAppById(Long id);
}
