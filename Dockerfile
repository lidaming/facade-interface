FROM java:openjdk-8
#WORKDIR ./
#RUN mvn -DskipTests=true package
COPY ./ruoyi-admin/target/ruoyi-admin.jar /app/
COPY ./entrypoint.sh /app/
ENTRYPOINT ["/bin/bash","/app/entrypoint.sh"]