package com.hnyc.token;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * @auther zero
 * @date 2023/3/12
 **/
public class TokenKeyHelper {
//    private static String tokenKey = "token";
//    private static String shopeeTokenKey = "token:shopee";

    public static String shopAccessTokenKey(long shopId) {
//        return String.format("%s:shop:access:%s", shopeeTokenKey, shopId);
        return geneShopeeToken("shop","access",shopId);
    }

    public static String shopRefreshTokenKey(long shopId) {
//        return String.format("%s:shop:refresh:%s", shopeeTokenKey, shopId);
        return geneShopeeToken("shop","refresh",shopId);
    }

    public static String merchantAccessKey(long merchantId) {
//        return String.format("%s:merchant:%s", shopeeTokenKey, merchantId);
        return geneShopeeToken("merchant","access",merchantId);
    }
    public static String merchantRefreshKey(long merchantId) {
//        return String.format("%s:merchant:%s", shopeeTokenKey, merchantId);
        return geneShopeeToken("merchant","refresh",merchantId);
    }
    public static String accountAccessKey(long accountId) {
//        return String.format("%s:account:access:%s", shopeeTokenKey, accountId);
        return geneShopeeToken("account","access",accountId);
    }

    public static String accountRefreshKey(long accountId) {
//        return String.format("%s:account:refresh:%s", shopeeTokenKey, accountId);
        return geneShopeeToken("account","refresh",accountId);
    }

    public static String geneShopeeToken(String classify, String ttype, Long id) {
        return geneToken("shopee",classify, ttype, id);
    }

    public static String geneToken(String pltName, String classify, String ttype, Long id) {
        return String.format("facade:token:%s:%s:%s:%s", pltName, classify, ttype, id);
    }
public static boolean isTokenKey(String cacheKey){
        return StringUtils.startsWith(cacheKey,"facade:token");
}
//token:facade:token:shopee:shop:access:52442
    public static ShopeeKeyItem keyItem(String cacheKey) {
        return new ShopeeKeyItem(cacheKey);
    }

    @Data
    public static class ShopeeKeyItem {
        private String sys;
        private String classify;
        private String pltName;
        private String tokenOfRole;
        private String tokenOfType;
        private String idOf;
//token:facade:token:shopee:shop:access:52442
        public ShopeeKeyItem(String key) {
            String[] ts = key.split(":");
            sys = ts[0];
            classify = ts[1];
            pltName = ts[2];
            tokenOfRole= ts[3];
            tokenOfType= ts[4];
            idOf=ts[5];
        }
    }
}


