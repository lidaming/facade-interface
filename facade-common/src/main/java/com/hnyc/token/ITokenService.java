package com.hnyc.token;

import com.hnyc.shopee.domain.AccessTokenResp;
import com.hnyc.shopee.domain.RefreshTokenResp;

/**
 * @auther zero
 * @date 2023/3/11
 **/
public interface ITokenService {
    AccessTokenResp getShopToken(String code,long shopId);
    AccessTokenResp getAccountToken(String code,long mainAccountId);
    RefreshTokenResp refreshShopToken(String refreshToken,long shopId);
    RefreshTokenResp refreshAccountToken(String refreshToken,long merchantId);

}
