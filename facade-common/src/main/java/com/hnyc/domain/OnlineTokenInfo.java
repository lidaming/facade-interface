package com.hnyc.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 线上平台的token信息：shopee,lazada等等对象 online_token_info
 *
 * @author ruoyi
 * @date 2023-03-13
 */
public class OnlineTokenInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    /**  */
    @Excel(name = "")
    private String createdBy;

    /**  */
    @Excel(name = "")
    private String updatedBy;

    /**  */
    @Excel(name = "")
    private String accessToken;

    /**  */
    @Excel(name = "")
    private String refreshToken;

    /**  */
    @Excel(name = "")
    private Long expireIn;

    /** accesstoken 的过期时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "accesstoken 的过期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expireAt;

    /** accesstoken 更新的时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "accesstoken 更新的时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date tokenAt;

    /** refreshtoken过期日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "refreshtoken过期日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date refreshtExpireAt;

    /** refreshtoken */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "refreshtoken", width = 30, dateFormat = "yyyy-MM-dd")
    private Date refreshtAt;

    /**  */
    @Excel(name = "")
    private Long refreshtExpireIn;

    /**  */
    @Excel(name = "")
    private String pltName;

    /**  */
    @Excel(name = "")
    private Long shopId;

    /**  */
    @Excel(name = "")
    private Long accountId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long merchantId;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCreatedAt(Date createdAt)
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt()
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt)
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt()
    {
        return updatedAt;
    }
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }
    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }
    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }

    public String getAccessToken()
    {
        return accessToken;
    }
    public void setRefreshToken(String refreshToken)
    {
        this.refreshToken = refreshToken;
    }

    public String getRefreshToken()
    {
        return refreshToken;
    }
    public void setExpireIn(Long expireIn)
    {
        this.expireIn = expireIn;
    }

    public Long getExpireIn()
    {
        return expireIn;
    }
    public void setExpireAt(Date expireAt)
    {
        this.expireAt = expireAt;
    }

    public Date getExpireAt()
    {
        return expireAt;
    }
    public void setTokenAt(Date tokenAt)
    {
        this.tokenAt = tokenAt;
    }

    public Date getTokenAt()
    {
        return tokenAt;
    }
    public void setRefreshtExpireAt(Date refreshtExpireAt)
    {
        this.refreshtExpireAt = refreshtExpireAt;
    }

    public Date getRefreshtExpireAt()
    {
        return refreshtExpireAt;
    }
    public void setRefreshtAt(Date refreshtAt)
    {
        this.refreshtAt = refreshtAt;
    }

    public Date getRefreshtAt()
    {
        return refreshtAt;
    }
    public void setRefreshtExpireIn(Long refreshtExpireIn)
    {
        this.refreshtExpireIn = refreshtExpireIn;
    }

    public Long getRefreshtExpireIn()
    {
        return refreshtExpireIn;
    }
    public void setPltName(String pltName)
    {
        this.pltName = pltName;
    }

    public String getPltName()
    {
        return pltName;
    }
    public void setShopId(Long shopId)
    {
        this.shopId = shopId;
    }

    public Long getShopId()
    {
        return shopId;
    }
    public void setAccountId(Long accountId)
    {
        this.accountId = accountId;
    }

    public Long getAccountId()
    {
        return accountId;
    }
    public void setMerchantId(Long merchantId)
    {
        this.merchantId = merchantId;
    }

    public Long getMerchantId()
    {
        return merchantId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("createdAt", getCreatedAt())
                .append("updatedAt", getUpdatedAt())
                .append("createdBy", getCreatedBy())
                .append("updatedBy", getUpdatedBy())
                .append("accessToken", getAccessToken())
                .append("refreshToken", getRefreshToken())
                .append("expireIn", getExpireIn())
                .append("expireAt", getExpireAt())
                .append("tokenAt", getTokenAt())
                .append("refreshtExpireAt", getRefreshtExpireAt())
                .append("refreshtAt", getRefreshtAt())
                .append("refreshtExpireIn", getRefreshtExpireIn())
                .append("pltName", getPltName())
                .append("shopId", getShopId())
                .append("accountId", getAccountId())
                .append("merchantId", getMerchantId())
                .toString();
    }
}
