package com.hnyc.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * shopee的店铺注册信息
 用户在oms点击授权的时候，回调后需要调用facade平台进行注册对象 shopee_shop_info
 *
 * @author ruoyi
 * @date 2023-03-12
 */
public class ShopeeShopInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    /**  */
    @Excel(name = "")
    private String createdBy;

    /**  */
    @Excel(name = "")
    private String updatedBy;

    /**  */
    @Excel(name = "")
    private Long onlineShopId;

    /**  */
    @Excel(name = "")
    private String onlineCode;

    /**  */
    @Excel(name = "")
    private String facadeUser;

    /**  */
    @Excel(name = "")
    private String callAppId;

    /**  */
    @Excel(name = "")
    private Long pltMerchantId;

    /**  */
    @Excel(name = "")
    private Long onlineAccountId;

    /** code 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "code 更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date codeAt;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String pltName;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCreatedAt(Date createdAt)
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt()
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt)
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt()
    {
        return updatedAt;
    }
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }
    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }
    public void setOnlineShopId(Long onlineShopId)
    {
        this.onlineShopId = onlineShopId;
    }

    public Long getOnlineShopId()
    {
        return onlineShopId;
    }
    public void setOnlineCode(String onlineCode)
    {
        this.onlineCode = onlineCode;
    }

    public String getOnlineCode()
    {
        return onlineCode;
    }
    public void setFacadeUser(String facadeUser)
    {
        this.facadeUser = facadeUser;
    }

    public String getFacadeUser()
    {
        return facadeUser;
    }
    public void setCallAppId(String callAppId)
    {
        this.callAppId = callAppId;
    }

    public String getCallAppId()
    {
        return callAppId;
    }
    public void setPltMerchantId(Long pltMerchantId)
    {
        this.pltMerchantId = pltMerchantId;
    }

    public Long getPltMerchantId()
    {
        return pltMerchantId;
    }
    public void setOnlineAccountId(Long onlineAccountId)
    {
        this.onlineAccountId = onlineAccountId;
    }

    public Long getOnlineAccountId()
    {
        return onlineAccountId;
    }
    public void setCodeAt(Date codeAt)
    {
        this.codeAt = codeAt;
    }

    public Date getCodeAt()
    {
        return codeAt;
    }
    public void setPltName(String pltName)
    {
        this.pltName = pltName;
    }

    public String getPltName()
    {
        return pltName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("createdAt", getCreatedAt())
                .append("updatedAt", getUpdatedAt())
                .append("createdBy", getCreatedBy())
                .append("updatedBy", getUpdatedBy())
                .append("onlineShopId", getOnlineShopId())
                .append("onlineCode", getOnlineCode())
                .append("facadeUser", getFacadeUser())
                .append("callAppId", getCallAppId())
                .append("pltMerchantId", getPltMerchantId())
                .append("onlineAccountId", getOnlineAccountId())
                .append("codeAt", getCodeAt())
                .append("pltName", getPltName())
                .toString();
    }
}
