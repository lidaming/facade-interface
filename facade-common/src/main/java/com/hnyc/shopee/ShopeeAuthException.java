package com.hnyc.shopee;

/**
 * @auther zero
 * @date 2023/3/12
 **/
public class ShopeeAuthException extends RuntimeException{
    public ShopeeAuthException(String msg){
        super(msg);
    }
}
