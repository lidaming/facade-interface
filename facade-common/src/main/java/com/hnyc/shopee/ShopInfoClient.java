package com.hnyc.shopee;

import com.hnyc.config.ShopeeConfig;
import com.hnyc.token.TokenCache;
import org.springframework.stereotype.Component;
import retrofit2.Call;
import retrofit2.http.GET;

import javax.annotation.Resource;

/**
 * @auther zero
 * @date 2023/3/19
 **/
@Component
public class ShopInfoClient {
    @Resource
    ShopeeConfig shopeeConfig;
    @Resource
    TokenCache tokenCache;
    // GET  /api/v2/shop/get_shop_info 获取商店信息
//    @GET("/api/v2/shop/get_shop_info")
    public void getShopInfo(String path,long shopId){
        String accessToken = tokenCache.getShopAccessToken(shopId);
        long timest = ShopeeUtils.timest();
        long partnerId = shopeeConfig.getPartnerId();
//        String url=String.format("%s%s?partner_id=%s&timestamp&access_token=%s&shop_id=%s&sign=%s",
//                shopeeConfig.getHost(),path, partnerId,
//                timest,
//                accessToken,
//                shopId,
//                ShopeeUtils.sign(path,timest,partnerId,shopeeConfig.getPartnerKey())
//                );
    }
}
