package com.hnyc.shopee.domain;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

/**
 * @auther zero
 * @date 2023/3/12
 **/
@Data
public class TokenResp {
    @JsonAlias(value = "access_token")
    @JSONField(name = "access_token")
    private String accessToken;
    @JsonAlias(value = "refresh_token")
    @JSONField(name = "refresh_token")
    private String refreshToken;
    @JsonAlias(value = "expire_in")
    @JSONField(name = "expire_in")
    private long expireIn;
    @JsonAlias(value = "request_id")
    @JSONField(name = "request_id")
    private String requestId;
    private String error;
    // 正常响应的时候，会携带字段
    private String message;
    // 异常响应的时候会携带的字段
    private String msg;
    public String getErrMsg(){
        return String.format("%s:%s",msg,message);
    }

}
