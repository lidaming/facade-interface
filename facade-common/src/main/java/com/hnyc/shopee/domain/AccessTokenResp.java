package com.hnyc.shopee.domain;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

import java.util.List;

/**
 * @auther zero
 * @date 2023/3/12
 **/
@Data
public class AccessTokenResp extends TokenResp {

    @JsonAlias(value = "merchant_id_list")
    @JSONField(name = "merchant_id_list")
    private List<Integer> merchantIdList;
    @JsonAlias(value = "shop_id_list")
    @JSONField(name = "shop_id_list")
    private List<Integer> shopIdList;
}
