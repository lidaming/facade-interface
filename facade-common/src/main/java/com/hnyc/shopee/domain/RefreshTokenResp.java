package com.hnyc.shopee.domain;

import lombok.Data;



/**
 * @auther zero
 * @date 2023/3/12
 **/
@Data
public class RefreshTokenResp extends TokenResp{
    private Integer merchantIdList;
    private Integer shopIdList;
}
