package com.hnyc.shopee;

import com.hnyc.entity.shopee.ShopInfo;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @auther zero
 * @date 2023/3/19
 **/
public interface ShopeeInfoClientV2 {
    @GET("/api/v2/shop/get_shop_info")
    Call<ShopInfo> shopInfo(@Query("shop_id") long shopId);
}
