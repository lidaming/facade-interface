package com.hnyc.service.impl;

import java.util.Date;
import java.util.List;

import com.hnyc.facade.domain.AuthCallbackParam;
import com.hnyc.token.TokenManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hnyc.mapper.ShopeeShopInfoMapper;
import com.hnyc.domain.ShopeeShopInfo;
import com.hnyc.service.IShopeeShopInfoService;

import javax.annotation.Resource;

/**
 * shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-12
 */
@Service
public class ShopeeShopInfoServiceImpl implements IShopeeShopInfoService 
{
    @Autowired
    private ShopeeShopInfoMapper shopeeShopInfoMapper;
    @Resource
    TokenManager tokenManager;

    /**
     * 查询shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册
     * 
     * @param id shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册主键
     * @return shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册
     */
    @Override
    public ShopeeShopInfo selectShopeeShopInfoById(Long id)
    {
        return shopeeShopInfoMapper.selectShopeeShopInfoById(id);
    }

    /**
     * 查询shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册列表
     * 
     * @param shopeeShopInfo shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册
     * @return shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册
     */
    @Override
    public List<ShopeeShopInfo> selectShopeeShopInfoList(ShopeeShopInfo shopeeShopInfo)
    {
        return shopeeShopInfoMapper.selectShopeeShopInfoList(shopeeShopInfo);
    }

    /**
     * 新增shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册
     * 
     * @param shopeeShopInfo shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册
     * @return 结果
     */
    @Override
    public int insertShopeeShopInfo(ShopeeShopInfo shopeeShopInfo)
    {
        return shopeeShopInfoMapper.insertShopeeShopInfo(shopeeShopInfo);
    }

    /**
     * 修改shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册
     * 
     * @param shopeeShopInfo shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册
     * @return 结果
     */
    @Override
    public int updateShopeeShopInfo(ShopeeShopInfo shopeeShopInfo)
    {
        return shopeeShopInfoMapper.updateShopeeShopInfo(shopeeShopInfo);
    }

    /**
     * 批量删除shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册
     * 
     * @param ids 需要删除的shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册主键
     * @return 结果
     */
    @Override
    public int deleteShopeeShopInfoByIds(Long[] ids)
    {
        return shopeeShopInfoMapper.deleteShopeeShopInfoByIds(ids);
    }

    /**
     * 删除shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册信息
     * 
     * @param id shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册主键
     * @return 结果
     */
    @Override
    public int deleteShopeeShopInfoById(Long id)
    {
        return shopeeShopInfoMapper.deleteShopeeShopInfoById(id);
    }

    @Override
    public int saveShopInfo(AuthCallbackParam param,String app) {

        ShopeeShopInfo query = new ShopeeShopInfo();
        query.setOnlineShopId(Long.valueOf(param.getShopId()));
        query.setOnlineAccountId(Long.valueOf(param.getMainAccountId()));
        query.setPltName(param.getPltName());
        //FIXME judge shop info
        List<ShopeeShopInfo> list = selectShopeeShopInfoList(query);
        if(list.size()>0) {
            // update
            ShopeeShopInfo shopeeShopInfo = list.get(0);
            shopeeShopInfo.setCallAppId(app);
            shopeeShopInfo.setFacadeUser(param.getSysUserId());
            shopeeShopInfo.setOnlineCode(param.getCode());
            shopeeShopInfo.setCodeAt(new Date());
            int i = updateShopeeShopInfo(shopeeShopInfo);
            if(i>0){
                tokenManager.getToken(shopeeShopInfo);
            }
            return i;
        }else{
            return insertShopInfoByParam(param,app);
        }



    }


    private int insertShopInfoByParam(AuthCallbackParam param,String app){
        ShopeeShopInfo shop = new ShopeeShopInfo();
        shop.setCallAppId(app);
        shop.setCreatedAt(new Date());
        shop.setFacadeUser(param.getSysUserId());

        if(param.getShopId()!=null){
            shop.setOnlineShopId(Long.valueOf(param.getShopId()));
        }
        if(param.getMainAccountId()!=null){
            shop.setOnlineAccountId(Long.valueOf(param.getMainAccountId()));
        }
        shop.setOnlineCode(param.getCode());
        shop.setCreateBy("api");
        int i = insertShopeeShopInfo(shop);
        if(i>0){
            tokenManager.getToken(shop);
        }
        return i;
    }
}
