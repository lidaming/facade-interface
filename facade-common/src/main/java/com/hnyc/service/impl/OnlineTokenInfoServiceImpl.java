package com.hnyc.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hnyc.mapper.OnlineTokenInfoMapper;
import com.hnyc.domain.OnlineTokenInfo;
import com.hnyc.service.IOnlineTokenInfoService;

/**
 * 线上平台的token信息：shopee,lazada等等Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-12
 */
@Service
public class OnlineTokenInfoServiceImpl implements IOnlineTokenInfoService 
{
    @Autowired
    private OnlineTokenInfoMapper onlineTokenInfoMapper;

    /**
     * 查询线上平台的token信息：shopee,lazada等等
     * 
     * @param id 线上平台的token信息：shopee,lazada等等主键
     * @return 线上平台的token信息：shopee,lazada等等
     */
    @Override
    public OnlineTokenInfo selectOnlineTokenInfoById(Long id)
    {
        return onlineTokenInfoMapper.selectOnlineTokenInfoById(id);
    }

    /**
     * 查询线上平台的token信息：shopee,lazada等等列表
     * 
     * @param onlineTokenInfo 线上平台的token信息：shopee,lazada等等
     * @return 线上平台的token信息：shopee,lazada等等
     */
    @Override
    public List<OnlineTokenInfo> selectOnlineTokenInfoList(OnlineTokenInfo onlineTokenInfo)
    {
        return onlineTokenInfoMapper.selectOnlineTokenInfoList(onlineTokenInfo);
    }

    /**
     * 新增线上平台的token信息：shopee,lazada等等
     * 
     * @param onlineTokenInfo 线上平台的token信息：shopee,lazada等等
     * @return 结果
     */
    @Override
    public int insertOnlineTokenInfo(OnlineTokenInfo onlineTokenInfo)
    {
        return onlineTokenInfoMapper.insertOnlineTokenInfo(onlineTokenInfo);
    }

    /**
     * 修改线上平台的token信息：shopee,lazada等等
     * 
     * @param onlineTokenInfo 线上平台的token信息：shopee,lazada等等
     * @return 结果
     */
    @Override
    public int updateOnlineTokenInfo(OnlineTokenInfo onlineTokenInfo)
    {
        return onlineTokenInfoMapper.updateOnlineTokenInfo(onlineTokenInfo);
    }

    /**
     * 批量删除线上平台的token信息：shopee,lazada等等
     * 
     * @param ids 需要删除的线上平台的token信息：shopee,lazada等等主键
     * @return 结果
     */
    @Override
    public int deleteOnlineTokenInfoByIds(Long[] ids)
    {
        return onlineTokenInfoMapper.deleteOnlineTokenInfoByIds(ids);
    }

    /**
     * 删除线上平台的token信息：shopee,lazada等等信息
     * 
     * @param id 线上平台的token信息：shopee,lazada等等主键
     * @return 结果
     */
    @Override
    public int deleteOnlineTokenInfoById(Long id)
    {
        return onlineTokenInfoMapper.deleteOnlineTokenInfoById(id);
    }
}
