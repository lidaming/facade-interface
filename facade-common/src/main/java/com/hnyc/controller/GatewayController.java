package com.hnyc.controller;

import com.alibaba.fastjson2.JSON;
import com.hnyc.common.BaseAppController;
import com.hnyc.facade.domain.AuthCallbackParam;

import com.hnyc.facade.domain.shopee.GoodsListReq;
import com.hnyc.entity.shopee.ShopInfo;
import com.hnyc.service.IShopeeShopInfoService;
import com.hnyc.shopee.ShopeeClientV2;
import com.hnyc.token.TokenManager;
import com.ruoyi.common.core.domain.AjaxResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @auther zero
 * @date 2023/3/11
 **/
@RestController
@RequestMapping("open/gateway")
@Slf4j
public class GatewayController extends BaseAppController {

    @Resource
    TokenManager tokenManager;
    @Resource
    IShopeeShopInfoService shopeeShopInfoService;
    @Resource
    ShopeeClientV2 shopeeClientV2;
    @PostMapping("shopee/register")
    public AjaxResult shopeeResister(@RequestBody AuthCallbackParam param){
       log.info("APP:{} \n shopee register:{}", getUsername(),JSON.toJSONString(param));
       if(param.getShopId()==null&&param.getMainAccountId()==null){
           return AjaxResult.error("shop id 和 main account id 同时为空");
       }
       int i= shopeeShopInfoService.saveShopInfo(param,getAppname());

        if(i>0){

            // 读取shop 信息
            // FIXME main_account_id
            ShopInfo shopInfo = shopeeClientV2.getShopInfo(Long.parseLong(param.getShopId()));
            // 访问系统传递过来的用户id
            shopInfo.setFacadeUser(param.getSysUserId());
            return AjaxResult.success(shopInfo);
        }
        return AjaxResult.error("保存失败");
    }

    @PostMapping("goods/list")
    public AjaxResult goodsList(@RequestBody GoodsListReq req){
           return AjaxResult.success(shopeeClientV2.goodsList(req));
    }
    @GetMapping("route")
    public AjaxResult route(@RequestHeader("route") String routePath){
        log.info(routePath);
        return AjaxResult.success();
    }
//    @Resource
//    ShopeeClientV2 shopeeClientV2;
//    @GetMapping("shopInfo")
//    public AjaxResult testShopInfo(@RequestParam("shop_id") long shopId){
//        return AjaxResult.success(shopeeClientV2.getShopInfo(shopId));
//    }
}
