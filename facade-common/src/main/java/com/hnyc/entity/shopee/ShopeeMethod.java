package com.hnyc.entity.shopee;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ShopeeMethod {

    //product
    Get_Item_List("/api/v2/product/get_item_list", "商品列表"),
    Search_Item("/api/v2/product/search_item", "搜索项目"),
    Get_Category("/api/v2/product/get_category", "类目数"),
    Get_Attributes("/api/v2/product/get_attributes", ""),
    Get_Brand_List("/api/v2/product/get_brand_list", ""),
    Get_Dts_Limit("/api/v2/product/get_dts_limit", ""),
    Get_Item_Limit("/api/v2/product/get_item_limit", ""),
    get_item_base_info("/api/v2/product/get_item_base_info", ""),
    get_item_extra_info("/api/v2/product/get_item_extra_info", ""),
    add_item("/api/v2/product/add_item", ""),
    update_item("/api/v2/product/update_item", ""),
    delete_item("/api/v2/product/delete_item", ""),
    init_tier_variation("/api/v2/product/init_tier_variation", ""),
    update_tier_variation("/api/v2/product/update_tier_variation", ""),
    get_model_list("/api/v2/product/get_model_list", ""),
    add_model("/api/v2/product/add_model", ""),
    update_model("/api/v2/product/update_model", ""),
    delete_model("/api/v2/product/delete_model", ""),
    support_size_chart("/api/v2/product/support_size_chart", ""),
    update_size_chart("/api/v2/product/update_size_chart", ""),
    unlist_item("/api/v2/product/unlist_item", ""),
    update_price("/api/v2/product/update_price", ""),
    update_stock("/api/v2/product/update_stock", ""),
    boost_item("/api/v2/product/boost_item", ""),
    get_boosted_list("/api/v2/product/get_boosted_list", ""),
    get_item_promotion("/api/v2/product/get_item_promotion", ""),
    update_sip_item_price("/api/v2/product/update_sip_item_price", ""),
    search_item("/api/v2/product/search_item", ""),
    get_comment("/api/v2/product/get_comment", ""),
    reply_comment("/api/v2/product/reply_comment", ""),
    category_recommend("/api/v2/product/category_recommend", ""),
    register_brand("/api/v2/product/register_brand", ""),
    get_recommend_attribute("/api/v2/product/get_recommend_attribute", ""),


    //order
    get_order_list("/api/v2/order/get_order_list", "获取所有订单"),
    get_shipment_list("/api/v2/order/get_shipment_list", "获取待发货订单"),
    get_order_detail("/api/v2/order/get_order_detail", "获取订单明细"),
    split_order("/api/v2/order/split_order", "拆单暂时不使用"),
    unsplit_order("/api/v2/order/unsplit_order", "拆单暂时不使用"),
    cancel_order("/api/v2/order/cancel_order", "用于卖家取消订单"),
    handle_buyer_cancellation("/api/v2/order/handle_buyer_cancellation", "用于处理买家取消订单的申请"),


    //Logistics

    get_shipping_parameter("/api/v2/logistics/get_shipping_parameter", "获取发货参数"),
    get_tracking_number("/api/v2/logistics/get_tracking_number", "轮询获取面单号"),
    ship_order("/api/v2/logistics/ship_order", "非集成渠道的订单，卖家应该准备好运单号，并在请求体中输入"),
    get_shipping_document_parameter("/api/v2/logistics/get_shipping_document_parameter", "打印面单 获得可选择的面单类型和建议的面单类型"),
    create_shipping_document("/api/v2/logistics/create_shipping_document", "为每个订单或包裹创建打印面单任务"),
    get_shipping_document_result("/api/v2/logistics/get_shipping_document_result", "获取打印面单任务结果"),
    download_shipping_document("/api/v2/logistics/download_shipping_document", "下载面单"),


    //return
    get_return_detail("/api/v2/returns/get_return_detail", ""),
    get_return_list("/api/v2/returns/get_return_list", "可获取一个店铺的退货退款申请列表"),


    ;

    private final String value;
    private final String desc;


}
