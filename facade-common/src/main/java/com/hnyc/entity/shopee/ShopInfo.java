package com.hnyc.entity.shopee;

import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * @auther zero
 * @date 2023/3/19
 **/
@Data
public class ShopInfo extends ShopeeResp {
    private long id;

    @JSONField(name = "shop_name",alternateNames = {"shop_name","shopName"})
    private String shopName;
    private String region;
//    applicable status: BANNED, FROZEN, NORMAL.
    private String status;
    @JSONField(name = "sip_affi_shop",alternateNames = {"sip_affi_shop","sipAffiShops"})
    private List<SipAffiShops> sipAffiShops;
    @JSONField(name = "is_cb",alternateNames = {"is_cb","isCB"})
    private boolean isCB;
    @JSONField(name = "is_cnsc",alternateNames = {"is_cnsc","isCNSC"})
    private boolean isCNSC;
    @JSONField(name = "shop_cbsc",alternateNames = {"shop_cbsc","shopCBSC"})
    private String shopCBSC;
    @JSONField(name = "request_id",alternateNames = {"request_id","requestId"})
    private String requestId;
    @JSONField(name = "auth_time",alternateNames = {"auth_time","authTime"})
    private long authTime;
    @JSONField(name = "expire_time",alternateNames = {"expire_time","expireTime"})
    private long expireTime;
    @JSONField(name = "is_sip",alternateNames = {"is_sip","isSIP"})
    private boolean isSIP;

    private String facadeUser;
}
