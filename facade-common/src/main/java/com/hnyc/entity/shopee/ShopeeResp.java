package com.hnyc.entity.shopee;

import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;

/**
 * @auther zero
 * @date 2023/3/19
 **/
@Data
public class ShopeeResp {
    @JSONField(name = "request_id")
    private String requestId;
    private String error;
    // 正常响应的时候，会携带字段
    private String message;
    // 异常响应的时候会携带的字段
    private String msg;
    public String getErrMsg(){
        return String.format("%s:%s",msg,message);
    }
}
