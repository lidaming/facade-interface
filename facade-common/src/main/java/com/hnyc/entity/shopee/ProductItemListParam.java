package com.hnyc.entity.shopee;

import com.hnyc.facade.domain.CommonReq;

public class ProductItemListParam extends CommonReq {



    @Override
    public String uri() {
        // route
        return ShopeeMethod.Get_Item_List.getValue();
    }
}
