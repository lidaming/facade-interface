package com.hnyc.entity.shopee;

import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;

/**
 * @auther zero
 * @date 2023/3/19
 **/
@Data
public class SipAffiShops {

    @JSONField(name = "sip_shop_id")
    private int sipShopId;

    private String region;
}
