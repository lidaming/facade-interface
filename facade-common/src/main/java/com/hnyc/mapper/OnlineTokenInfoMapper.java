package com.hnyc.mapper;

import java.util.List;
import com.hnyc.domain.OnlineTokenInfo;

/**
 * 线上平台的token信息：shopee,lazada等等Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-12
 */
public interface OnlineTokenInfoMapper 
{
    /**
     * 查询线上平台的token信息：shopee,lazada等等
     * 
     * @param id 线上平台的token信息：shopee,lazada等等主键
     * @return 线上平台的token信息：shopee,lazada等等
     */
    public OnlineTokenInfo selectOnlineTokenInfoById(Long id);

    /**
     * 查询线上平台的token信息：shopee,lazada等等列表
     * 
     * @param onlineTokenInfo 线上平台的token信息：shopee,lazada等等
     * @return 线上平台的token信息：shopee,lazada等等集合
     */
    public List<OnlineTokenInfo> selectOnlineTokenInfoList(OnlineTokenInfo onlineTokenInfo);

    /**
     * 新增线上平台的token信息：shopee,lazada等等
     * 
     * @param onlineTokenInfo 线上平台的token信息：shopee,lazada等等
     * @return 结果
     */
    public int insertOnlineTokenInfo(OnlineTokenInfo onlineTokenInfo);

    /**
     * 修改线上平台的token信息：shopee,lazada等等
     * 
     * @param onlineTokenInfo 线上平台的token信息：shopee,lazada等等
     * @return 结果
     */
    public int updateOnlineTokenInfo(OnlineTokenInfo onlineTokenInfo);

    /**
     * 删除线上平台的token信息：shopee,lazada等等
     * 
     * @param id 线上平台的token信息：shopee,lazada等等主键
     * @return 结果
     */
    public int deleteOnlineTokenInfoById(Long id);

    /**
     * 批量删除线上平台的token信息：shopee,lazada等等
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOnlineTokenInfoByIds(Long[] ids);
}
