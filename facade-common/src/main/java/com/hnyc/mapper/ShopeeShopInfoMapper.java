package com.hnyc.mapper;

import java.util.List;
import com.hnyc.domain.ShopeeShopInfo;

/**
 * shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-12
 */
public interface ShopeeShopInfoMapper 
{
    /**
     * 查询shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册
     * 
     * @param id shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册主键
     * @return shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册
     */
    public ShopeeShopInfo selectShopeeShopInfoById(Long id);

    /**
     * 查询shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册列表
     * 
     * @param shopeeShopInfo shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册
     * @return shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册集合
     */
    public List<ShopeeShopInfo> selectShopeeShopInfoList(ShopeeShopInfo shopeeShopInfo);

    /**
     * 新增shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册
     * 
     * @param shopeeShopInfo shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册
     * @return 结果
     */
    public int insertShopeeShopInfo(ShopeeShopInfo shopeeShopInfo);

    /**
     * 修改shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册
     * 
     * @param shopeeShopInfo shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册
     * @return 结果
     */
    public int updateShopeeShopInfo(ShopeeShopInfo shopeeShopInfo);

    /**
     * 删除shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册
     * 
     * @param id shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册主键
     * @return 结果
     */
    public int deleteShopeeShopInfoById(Long id);

    /**
     * 批量删除shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteShopeeShopInfoByIds(Long[] ids);
}
