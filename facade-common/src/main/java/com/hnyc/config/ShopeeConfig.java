package com.hnyc.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @auther zero
 * @date 2023/3/11
 **/
@Data
@Configuration
@ConfigurationProperties(prefix = "shopee")
public class ShopeeConfig {
    private String host;
    private long partnerId;
    private String partnerKey;
}
