# 各类id说明

main_account_id -> merchant_id -> shop_id

# token 加载的逻辑
> https://open.shopee.com/developer-guide/20
1. 授权后使用token+[shop_id|main_account_id]来读取具体的access_token；
2. access_token 过期前使用refresh_token 来更新access_token
3. refresh接口每次调用，都会返回一个新的refresh_token