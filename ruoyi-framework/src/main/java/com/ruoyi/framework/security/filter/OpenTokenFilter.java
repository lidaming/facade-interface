package com.ruoyi.framework.security.filter;

import com.ruoyi.common.core.domain.model.CurrentApp;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.security.token.OpenAuthenticationToken;
import com.ruoyi.framework.web.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * token过滤器 验证token有效性
 * 
 * @author ruoyi
 */
@Component
public class OpenTokenFilter extends OncePerRequestFilter
{
    @Autowired
    private TokenService tokenService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException
    {
        if(!request.getRequestURI().contains("/open/")){
            chain.doFilter(request,response);
        }else {
            CurrentApp app = tokenService.getOpenApp(request);
            if (StringUtils.isNotNull(app) && StringUtils.isNull(SecurityUtils.getAuthentication()))
            {
//            tokenService.verifyToken(loginUser);
                OpenAuthenticationToken authenticationToken = new OpenAuthenticationToken(app, null, null);
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }
            chain.doFilter(request, response);
        }

    }
}
