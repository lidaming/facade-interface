package com.hnyc;

/**
 * 系统内实现，获取当前登录的用户id
 * @auther zero
 * @date 2023/3/12
 **/
public interface IFacadeUser {
    /**
     * 读取用户id
     * @return
     */
    String getSysUserIdentify();
}
