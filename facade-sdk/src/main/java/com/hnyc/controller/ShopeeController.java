package com.hnyc.controller;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.hnyc.IFacadeUser;
import com.hnyc.IRegisterShopCallback;
import com.hnyc.common.FacadeClient;
import com.hnyc.config.ShopeeConfig;
import com.hnyc.domain.RespResult;
import com.hnyc.facade.domain.AuthCallbackParam;
import com.hnyc.facade.shopee.ShopInfo;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * @auther zero
 * @date 2023/3/11
 **/
@Slf4j
@RestController
@RequestMapping("facade/shopee")
public class ShopeeController {
    @Resource
    ShopeeConfig shopeeConfig;
    @Resource
    IFacadeUser facadeUser;

    @Resource
    FacadeClient facadeClient;
    @Resource
    IRegisterShopCallback callback;
    @GetMapping("authUrl")
    public String auth()  {
        return shopeeConfig.authUrl();
    }

    /**
     * 需要开放授权 此地址
     * @param code

     * @param shopId
     * @param mainAccountId
     * @return
     */
    @GetMapping("recvCode")
    public String recvCode(String code,
                           @RequestParam(name="shop_id",required = false) String shopId,
                           @RequestParam(name="main_account_id",required = false) String mainAccountId){
        log.info("shopee callback:\n \tcode:{} \n\tshop_id:{} \n\tmain_account_id:{}",code,shopId,mainAccountId);
        if(StringUtils.isEmpty(shopId)&&StringUtils.isEmpty(mainAccountId)){
            return "shope_id 和main_account_id不能同时为空";
        }
        AuthCallbackParam params = new AuthCallbackParam(code, shopId, mainAccountId, facadeUser.getSysUserIdentify(),"shopee");
        facadeClient.registerShop(params, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // todo 请求失败，打印日志
                log.error("注册shop失败：",e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // 请求成功，存储结果
                if(response.code()== HttpStatus.OK.value()){
                    RespResult respResult = JSON.parseObject(response.body().bytes(), RespResult.class);
                    log.info("响应结果：{}",JSON.toJSONString(respResult));
                    // 回调内容

                    if(callback!=null){
                        callback.registerCallback(JSONObject.parseObject(respResult.get("data").toString(), ShopInfo.class));
                    }else {
                        log.info("未实现注册回调接口");
                    }
                }else{
                    log.error("响应状态码异常：{}\n{}",response.code(), JSON.toJSONString(response.body()));
                }
            }
        });
        return "success";
    }
}
