package com.hnyc.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @auther zero
 * @date 2023/3/12
 **/
@Data
@Configuration
@ConfigurationProperties(prefix = "facade")
public class FacadeConfig {
    private String domain;
    private String user;
    private String token;
}
