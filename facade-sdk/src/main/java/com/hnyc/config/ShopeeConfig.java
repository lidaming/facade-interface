package com.hnyc.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigInteger;

/**
 * @auther zero
 * @date 2023/3/11
 **/
@Data
@Configuration
@ConfigurationProperties(prefix = "shopee")
public class ShopeeConfig {
    private String host;
    private String partnerId;
    private String partnerKey;
    private String callbackUrl;
    public String authUrl(){
        long timest = System.currentTimeMillis() / 1000L;

        String path = "/api/v2/shop/auth_partner";


        String tmp_base_string = String.format("%s%s%s", partnerId, path, timest);
        byte[] partner_key;
        byte[] base_string;
        String sign = "";
        try {
            base_string = tmp_base_string.getBytes("UTF-8");
            partner_key = partnerKey.getBytes("UTF-8");
            Mac mac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(partner_key, "HmacSHA256");
            mac.init(secret_key);
            sign = String.format("%064x", new BigInteger(1, mac.doFinal(base_string)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return host + path + String.format("?partner_id=%s&timestamp=%s&sign=%s&redirect=%s",partnerId, timest, sign, callbackUrl);

    }
    public String apiVersion(){
        return "v2";
    }

}
