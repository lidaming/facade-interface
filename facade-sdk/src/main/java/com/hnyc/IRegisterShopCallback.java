package com.hnyc;

import com.hnyc.facade.shopee.ShopInfo;

public interface IRegisterShopCallback {
    void registerCallback(ShopInfo shopInfo);
}
