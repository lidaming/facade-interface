package com.hnyc.hole;

import com.hnyc.IRegisterShopCallback;
import com.hnyc.facade.shopee.ShopInfo;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Component;

@ConditionalOnMissingBean(IRegisterShopCallback.class)
@Component
public class RegisterCallbackHole implements IRegisterShopCallback {

    @Override
    public void registerCallback(ShopInfo shopInfo) {
        throw new RuntimeException("not supperted IRegisterShopCallback");
    }
}
