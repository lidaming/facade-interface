package com.hnyc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @auther zero
 * @date 2023/3/11
 **/
@SpringBootApplication
public class SdkApp {
    public static void main(String[] args) {
        SpringApplication.run(SdkApp.class);
    }
}
