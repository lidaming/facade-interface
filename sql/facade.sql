/*==============================================================*/
/* Table: online_plt_config                                     */
/*==============================================================*/
create table online_plt_config
(
    id                   bigint not null auto_increment,
    created_at           datetime,
    updated_at           datetime,
    created_by           varchar(200),
    updated_by           varchar(200),
    plt_name             varchar(200),
    plt_url              varchar(200),
    app_id               varchar(200),
    app_secret           varchar(200),
    plt_domain           varchar(200),
    primary key (id)
);

/*==============================================================*/
/* Table: online_plt_auth_state                                 */
/*==============================================================*/
create table online_plt_auth_state
(
    id                   bigint not null auto_increment,
    created_at           datetime,
    updated_at           datetime,
    access_token         varchar(200),
    expired_at           datetime,
    online_plt_name      varchar(200),
    online_plt_id        bigint,
    refresh_token        varchar(200),
    primary key (id)
);
/*==============================================================*/
/* Table: online_interface                                      */
/*==============================================================*/
create table online_interface
(
    id                   bigint not null auto_increment,
    created_at           datetime,
    updated_at           datetime,
    created_by           varchar(200),
    updated_by           varchar(200),
    req_path             varchar(200),
    forward_path         varchar(200),
    forward_method       varchar(20) comment 'GET,POST',
    forward_protocol     varchar(20) comment 'http,https',
    params_map           varchar(2000) comment '转发时候的参数映射',
    forward_plt          varchar(20),
    forward_plt_id       bigint,
    resp_map             varchar(2000),
    req_type             varchar(20) comment 'array,object',
    resp_type            varchar(20) comment 'array,object',
    primary key (id)
);
/*==============================================================*/
/* Table: forward_call_log                                      */
/*==============================================================*/
create table forward_call_log
(
    id                   bigint not null auto_increment,
    created_at           datetime,
    created_by           varchar(200),
    req_path             varchar(200),
    forward_path         varchar(200),
    req_params           varchar(2000),
    forward_params       varchar(2000),
    forward_resp         varchar(2000),
    req_resp             varchar(2000),
    src_plt              varchar(200),
    src_plt_id           bigint,
    primary key (id)
);
