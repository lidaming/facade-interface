create table file_info
(
    id                 int auto_increment
        primary key,
    file_name          varchar(300)  null,
    file_path          varchar(500)  null,
    created_at         datetime      null,
    file_status        int default 1 null comment '0 删除 1 正常',
    new_file_name      varchar(200)  null,
    original_file_name varchar(400)  null
);
create table open_app
(
    id              int auto_increment
        primary key,
    app_name        varchar(200)  not null,
    app_description varchar(500)  null,
    app_token       varchar(60)   null,
    created_at      datetime      not null,
    created_by      varchar(50)   not null,
    app_status      int default 1 not null comment '0 禁用 1 启用',
    expire_time     datetime      null comment '过期时间'
)
    comment '申请权限的应用';