drop table if exists shopee_shop_info;

/*==============================================================*/
/* Table: shopee_shop_info                                      */
/*==============================================================*/
create table shopee_shop_info
(
   id                   bigint not null auto_increment,
   created_at           datetime,
   updated_at           datetime,
   created_by           varchar(200),
   updated_by           varchar(200),
   online_shop_id       bigint,
   online_code          varchar(200),
   facade_user          varchar(30),
   call_app_id          varchar(30),
   plt_merchant_id      bigint,
   online_account_id    bigint,
   code_at              datetime comment 'code 更新时间',
   primary key (id)
);
drop table if exists online_token_info;

/*==============================================================*/
/* Table: online_token_info                                     */
/*==============================================================*/
create table online_token_info
(
   id                   bigint not null auto_increment,
   created_at           datetime,
   updated_at           datetime,
   created_by           varchar(200),
   updated_by           varchar(200),
   access_token         varchar(200),
   refresh_token        varchar(200),
   expire_in            bigint,
   expire_at            datetime comment 'accesstoken 的过期时间',
   token_at             datetime comment 'accesstoken 更新的时间',
   refresht_expire_at   datetime comment 'refreshtoken过期日期',
   refresht_at          datetime comment 'refreshtoken',
   refresht_expire_in   bigint,
   primary key (id)
);

alter table online_token_info comment '线上平台的token信息：shopee,lazada等等';


alter table shopee_shop_info comment 'shopee的店铺注册信息
用户在oms点击授权的时候，回调后需要调用facade平台进行注册';

