package com.hnyc.facade.domain;

import lombok.Data;
import okhttp3.Request;

@Data
public abstract class CommonReq extends FacadeUrl {
    private String shopId;
    private String pltName;
    public Request.Builder reqBuilder(){
        return new Request.Builder()
                .addHeader("route",uri());
    }
    public abstract String uri();
    public String url(){
        return String.format("%s%s",getDomain(),uri());
    }
}
