package com.hnyc.facade.domain;


import com.alibaba.fastjson2.annotation.JSONField;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.Data;

@Data

public abstract class FacadeUrl {
    @JSONField(serialize = false)
    private String domain;
    @JSONField(serialize = false)
    private String user;
    @JSONField(serialize = false)
    private String token;

    public abstract String url();
}
