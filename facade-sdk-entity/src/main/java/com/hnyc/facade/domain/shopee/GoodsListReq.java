package com.hnyc.facade.domain.shopee;

import com.alibaba.fastjson2.annotation.JSONField;
import com.hnyc.facade.domain.shopee.PageParam;
import lombok.Data;

import java.time.LocalDateTime;
@Data
public class GoodsListReq extends PageParam {

    @JSONField(name = "shop_id")
    private long shopId;

    private String pltName;

    @JSONField(name = "item_status")
    private String itemStatus;

//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
//    private LocalDateTime update_time_from;
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
//    private LocalDateTime update_time_to;
}
