package com.hnyc.facade.domain.shopee;

import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;

@Data
public class PageParam {
    private int offset;
    @JSONField(name = "page_size")
    private int pageSize;
}
