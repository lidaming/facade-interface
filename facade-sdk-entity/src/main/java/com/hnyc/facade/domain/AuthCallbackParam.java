package com.hnyc.facade.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @auther zero
 * @date 2023/3/12
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthCallbackParam extends FacadeUrl {
    private String code;
    private String shopId;
    private String mainAccountId;
    // oms 的用户标识
    private String sysUserId;
    private String pltName;

    public AuthCallbackParam(String domain, String user, String token) {
        super.setDomain(domain);
        super.setUser(user);
        super.setToken(token);
    }

    @Override
    public String url() {
        return String.format("%s/open/gateway/shopee/register",getDomain());
    }
}
