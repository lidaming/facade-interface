package com.ruoyi.common.utils;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;

public class HttpParamsUtil {
    public static String toQuery(JSONObject json){
        StringBuilder sb=new StringBuilder();
        for (String key:  json.keySet()) {
            sb.append(key);
            sb.append("=");
            sb.append(json.get(key));
            sb.append("&");
        }
        return sb.subSequence(0,sb.length()-1).toString();
    }
    public static String objectToQuery(Object o){
        return toQuery(JSONObject.parse(JSON.toJSONString(o)));
    }
}
