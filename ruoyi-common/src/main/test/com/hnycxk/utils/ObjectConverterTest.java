package com.hnycxk.utils;


import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.alibaba.fastjson2.JSONWriter;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.util.Converter;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.ruoyi.common.utils.ObjectConverter;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.ObjectUtils;
import org.junit.Test;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ObjectConverterTest {
    @Test
    public void TestConvert() {
        JSONObject config = JSONObject.of();
        config.put("id", JSONObject.of("key","id","type","v"));
        config.put("itemId", JSONObject.of("key","item.id","type","v"));
        config.put("name", JSONObject.of("key","gName","type","v"));
        config.put("itemName", JSONObject.of("key","item.name","type","v"));
        //
        JSONObject itemConfig = JSONObject.of("key", "list","type","list");

        JSONObject itemMap = JSONObject.of();
        itemMap.put("orderId", JSONObject.of("key","id","type","v"));
        itemMap.put("orderName",JSONObject.of("key", "name","type","v"));
        itemConfig.put("map", itemMap);

        config.put("list", itemConfig);


        SourceObj source = new SourceObj();
        source.setgName("i am gname");
        source.setId(2l);

        ItemObj item = new ItemObj();
        item.setId(1);
        item.setName("i am item");
        source.setItem(item);

        ArrayList<ItemObj> list = Lists.newArrayList();
        ItemObj e = new ItemObj();
        e.setName("list item");
        e.setId(11);
        list.add(e);
        source.setList(list);

        TargetObj target = ObjectConverter.convert(config, source, TargetObj.class);
        System.out.println(JSON.toJSONString(target));
    }


    public static class TargetItem {
        private int orderId;
        private String orderName;

        public int getOrderId() {
            return orderId;
        }

        public void setOrderId(int orderId) {
            this.orderId = orderId;
        }

        public String getOrderName() {
            return orderName;
        }

        public void setOrderName(String orderName) {
            this.orderName = orderName;
        }
    }

//    @JsonDeserialize(using = TestJsonDeserializer.class)
    public static class TargetObj {
//        @JsonDeserialize(using = TestJsonDeserializer.class)
        private long id;
        private int itemId;
        private String name;
        private String itemName;
        private List<TargetItem> list;

        public List<TargetItem> getList() {
            return list;
        }

        public void setList(List<TargetItem> list) {
            this.list = list;
        }

        public long getId() {
            return id;
        }

//        @JsonDeserialize(using = TestJsonDeserializer.class)
        public void setId(long id) {
            this.id = id;
        }

        public int getItemId() {
            return itemId;
        }

        public void setItemId(int itemId) {
            this.itemId = itemId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }
    }

    public static class ItemObj {
        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class SourceObj {
        private long id;
        private String gName;
        private ItemObj item;
        private List<ItemObj> list;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getgName() {
            return gName;
        }

        public void setgName(String gName) {
            this.gName = gName;
        }

        public ItemObj getItem() {
            return item;
        }

        public void setItem(ItemObj item) {
            this.item = item;
        }

        public List<ItemObj> getList() {
            return list;
        }

        public void setList(List<ItemObj> list) {
            this.list = list;
        }
    }

    @Test
    public void testAnnotationConvert() {

        SourceObj source = new SourceObj();
        source.setgName("i am gname");
        source.setId(2l);

        ItemObj item = new ItemObj();
        item.setId(1);
        item.setName("i am item");
        source.setItem(item);

        ArrayList<ItemObj> list = Lists.newArrayList();
        ItemObj e = new ItemObj();
        e.setName("list item");
        e.setId(11);
        list.add(e);
        source.setList(list);

        TargetObj targetObj = JSONObject.parseObject(JSON.toJSONString(source), TargetObj.class);
        System.out.println(JSON.toJSONString(targetObj));
    }

    public static class TestJsonDeserializer extends JsonDeserializer {

        @Override
        public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            if (ObjectUtils.isEmpty(jsonParser)) {
                return null;
            }

            return null;
        }
    }
    public static class TestGsonDeserialize implements   com.google.gson.JsonDeserializer<TargetObj>{



        @Override
        public TargetObj deserialize(JsonElement json, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            JsonObject jsonObject = json.getAsJsonObject();



            LocalDate localDate = LocalDate.of(
                    jsonObject.get("year").getAsInt(),
                    jsonObject.get("month").getAsInt(),
                    jsonObject.get("day").getAsInt()
            );

            return null;
        }
    }
}
