import request from '@/utils/request'

// 查询申请权限的应用列表
export function listApp(query) {
  return request({
    url: '/system/app/list',
    method: 'get',
    params: query
  })
}

// 查询申请权限的应用详细
export function getApp(id) {
  return request({
    url: '/system/app/' + id,
    method: 'get'
  })
}

// 新增申请权限的应用
export function addApp(data) {
  return request({
    url: '/system/app',
    method: 'post',
    data: data
  })
}

// 修改申请权限的应用
export function updateApp(data) {
  return request({
    url: '/system/app',
    method: 'put',
    data: data
  })
}

// 删除申请权限的应用
export function delApp(id) {
  return request({
    url: '/system/app/' + id,
    method: 'delete'
  })
}
