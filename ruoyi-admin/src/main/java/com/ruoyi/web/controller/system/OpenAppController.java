package com.ruoyi.web.controller.system;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.uuid.IdUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.OpenApp;
import com.ruoyi.system.service.IOpenAppService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 申请权限的应用Controller
 * 
 * @author ruoyi
 * @date 2023-02-26
 */
@RestController
@RequestMapping("/system/app")
public class OpenAppController extends BaseController
{
    @Autowired
    private IOpenAppService openAppService;


    /**
     * 查询申请权限的应用列表
     */
    @PreAuthorize("@ss.hasPermi('system:app:list')")
    @GetMapping("/list")
    public TableDataInfo list(OpenApp openApp)
    {
        startPage();
        List<OpenApp> list = openAppService.selectOpenAppList(openApp);
        return getDataTable(list);
    }

    /**
     * 导出申请权限的应用列表
     */
    @PreAuthorize("@ss.hasPermi('system:app:export')")
    @Log(title = "申请权限的应用", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OpenApp openApp)
    {
        List<OpenApp> list = openAppService.selectOpenAppList(openApp);
        ExcelUtil<OpenApp> util = new ExcelUtil<OpenApp>(OpenApp.class);
        util.exportExcel(response, list, "申请权限的应用数据");
    }

    /**
     * 获取申请权限的应用详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:app:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(openAppService.selectOpenAppById(id));
    }

    /**
     * 新增申请权限的应用
     */
    @PreAuthorize("@ss.hasPermi('system:app:add')")
    @Log(title = "申请权限的应用", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OpenApp openApp)
    {
        LoginUser user = (LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        openApp.setCreatedBy(user.getUsername());
        openApp.setCreatedAt(new Date());
        openApp.setAppToken(IdUtils.fastSimpleUUID());

        int rows = openAppService.insertOpenApp(openApp);


        return toAjax(rows);
    }

    /**
     * 修改申请权限的应用
     */
    @PreAuthorize("@ss.hasPermi('system:app:edit')")
    @Log(title = "申请权限的应用", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OpenApp openApp)
    {
        return toAjax(openAppService.updateOpenApp(openApp));
    }

    /**
     * 删除申请权限的应用
     */
    @PreAuthorize("@ss.hasPermi('system:app:remove')")
    @Log(title = "申请权限的应用", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(openAppService.deleteOpenAppByIds(ids));
    }
}
